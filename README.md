## Pokemon Battle Simulator

### Purpose
The purpose of this program is to enable users to simulate Pokemon battles using a command-line interface. Users can assemble teams of Pokemon and battle them against each other. The program provides different battle modes, such as Set Mode, Rotating Mode, and Optimized Mode, each with its own rules and strategies. Additionally, the program introduces a special Pokemon called MissingNo with unique abilities.

### Task Descriptions
1. **ABC & Its Children**: Defining an abstract class `PokemonBase` and child classes for three types of Pokemon.
2. **Assembling the Team**: Implementing a class `PokeTeam` to assemble teams of Pokemon.
#### Usage
```python
Howdy Trainer! Choose your team as C B S
where C is the number of Charmanders
B is the number of Bulbasaurs
S is the number of Squirtles
> 2 3 1
print(team)
"Charmander's HP = 7 and level = 1, Bulbasaur's HP = 9 and level = 1, Squirtle's HP = 8 and level = 1"
```
3. **Set Mode Battle**: Creating a battle mode where Pokemon fight until one faints.
4. **Rotating Mode Battle**: Implementing a battle mode where Pokemon rotate after each round.
5. **Optimised Mode Battle**: Introducing a battle mode where Pokemon are ordered based on chosen attributes.
6. **MissingNo Appears!**: Defining a new Pokemon class `GlitchMon` with special abilities, including a child class `MissingNo`.

### Implementation
The program consists of several Python files:
- `pokemon_base.py`: Contains the abstract class `PokemonBase` and its child classes for specific Pokemon types.
- `pokemon.py`: Implements the `PokeTeam` class for assembling Pokemon teams.
- `battle.py`: Defines the `Battle` class for different battle modes and interactions between teams.
- `referential_array.py`, `stack_adt.py`, `queue_adt.py`, `array_sorted_list.py`, `sorted_list.py`: Provided ADT files.

### Usage
1. Clone the repository to your local machine.
2. Ensure you have Python installed.
3. Run the program using Python with appropriate command-line arguments.

### Testing
Extensive unit tests have been implemented for each method and class.

### Documentation
Each file, class, function, and method is appropriately documented to provide clarity on its purpose, inputs, and outputs.

### Complexity Analysis
The best and worst case complexity of each method is provided in the docstrings to help understand the algorithmic efficiency of the program.

For further details, refer to the provided task specifications and instructions. Happy battling!
