"""
Battle Class, inheriting from the Pokemon Classes and the PokeTeam Class. This class is where the battle is performed, using set_mode_batlle, when battle mode is 0, then a battle between two trainers takes place until one of the two trainers' teams is empty.
"""

__author__ = "Nabiha Siddiqui, Aiguo Shao, Anushka Reddy"
from queue import Queue
from queue_adt import CircularQueue
from sorted_list import ListItem
from array_sorted_list import ArraySortedList
from poke_team import PokeTeam

from pokemon import GlitchMon

class Battle():
    """Class performing the battle and declaring the winner when battle mode is 0"""
    def __init__(self, trainer_one_name: str, trainer_two_name: str) -> None:
        """Initialization"""
        self.trainer_one_name=trainer_one_name
        self.trainer_two_name=trainer_two_name
        self.team1=[]
        self.team2=[]
        self.battle_mode=None
        PokeTeam.__init__(self, self.trainer_one_name)
        PokeTeam.__init__(self, self.trainer_two_name)



    def set_mode_battle(self) -> str:
        """Sets the battle mode as 0, and creates two teams based off user input which then battle until
        at least one team is empty, and then declares the winner.
        :pre: teams cannot be empty before battle has started
        :raises: ValueError when at least one of teams is empty before battle
        :complexity: O(1) for best case, where there is only one pokemon in one team, and that pokemon faints, this means
        the while loop will only execute once
        O(n) for worst case, where n is the number of pokemon in the highest of the two teams, where the while loop will iterate
        and each pokemon will have to faint until the team is empty"""


        self.battle_mode = 0

        # creating the first team
        PokeTeam.choose_team(PokeTeam, 0)
        c, b, s = Battle.set_pokemon(self)
        self.team1 = PokeTeam.assign_team(self, c, b, s)

        # creating the second team
        PokeTeam.choose_team(PokeTeam, 0)
        c, b, s = Battle.set_pokemon(self)
        self.team2 = PokeTeam.assign_team(self, c, b, s)

        # if one of the teams is empty before the battle, raise a ValueError
        if self.team_is_empty(self.team1, self.team2):
            raise ValueError("Teams cannot be empty before battle")

        #battle continues until one team is empty
        while not self.team_is_empty(self.team1,self.team2):

            # pokemon battling in team 1 is at the top of the stack and are assigned to poke1
            poke1=self.team1.pop()
            p1 = poke1 #creates a copy of poke1
            self.poke1=poke1

            # pokemon battling in team 2 is at the top of the stack and are assigned to poke2
            poke2=self.team2.pop()
            p2 = poke2 #creates a copy of poke2
            self.poke2=poke2

            #method to decide which pokemon is an attacker and which is a defender
            self.set_attacker_defender(p1,p2)


        #call battle result to determine who won or if the battle resulted in a draw
        return self.battle_result(self.team1,self.team2)

    def battle_attacker_defender(self, attacker, defender, p1, p2) -> None:
        """This code calculates the damage taken by the defender pokemon from the attack of the attacker pokemon
        and then checks if it's still alive. If the defender is still alive, the program calculates the damage taken
        by the attacker after being attacked by defender. If the defender faints, the attackers gains a level and the defender is removed from its team.
        If the attacker faints from the defenders attack, the defender gains a level and the attacker is removed from its team. If
        both pokemon are still alive, both pokemon lose 1 hp, and then the program checks if either has fainted, if one has fainted it is removed from its team,
        while the other gains a level. If neither faint, then both are removed from their teams.
        O(1) for best when battle mode =0 and O(nlogn) for worst when battle mode =2.
        """
        #attacker will attack first, so we need to calculate the hp of the defender pokemon after the attack
        defender_hp = defender.get_damage_after_attack(attacker.get_poke_type(), attacker.get_level())

        if defender_hp > 0:
            # if the defenders hp is more than 0, we need to set the hp of the pokemon so that it is saved
            self.set_defender_hp(defender, p1, p2, defender_hp)
            #if the defender pokemon is still alive, we need to calculate the hp of the attacker pokemon after the defender pokemon attacks it
            attacker_hp = attacker.get_damage_after_attack(defender.get_poke_type(), defender.get_level())

            if attacker_hp > 0:
                # if the attackers hp is more than 0, we need to set the hp of the pokemon so that it is saved
                self.set_attacker_hp(attacker, p1, p2, attacker_hp)

                #if the attacker is still alive, both pokemon lose 1 hp
                attacker_hp=Battle.reduce_hp(attacker)
                defender_hp=Battle.reduce_hp(defender)
                #saving these new hps
                self.set_defender_hp(defender, p1, p2, defender_hp)
                self.set_attacker_hp(attacker, p1, p2, attacker_hp)

                #if attacker is alive and defender has fainted, attacker will gain one level and the defender will be eliminated from its team
                if attacker_hp > 0 and defender_hp == 0:
                    self.attacker_wins_round(attacker, defender, p1, p2)

                # if defender is alive and attacker has fainted, defender will gain one level and the attacker will be eliminated from its team
                elif attacker_hp == 0 and defender_hp > 0:
                    self.defender_wins_round(attacker, defender, p1, p2)

                # if both have fainted, both will be removed from their teams
                elif attacker_hp == 0 and defender_hp == 0:
                    self.both_faint()

                # #if both alive, the item will be deleted and add again
                elif attacker_hp > 0 and defender_hp > 0:
                    self.set_defender_hp(defender, p1, p2, defender_hp)
                    self.set_attacker_hp(attacker, p1, p2, attacker_hp)
                    self.both_alive()

            #if attacker has fainted, defender will gain one level and attacker will be eliminated from its team
            else:
                self.defender_wins_round(attacker, defender, p1, p2)

            #if defender has fainted, attacker will gain one level and defender will be eliminated from its team
        else:
            self.attacker_wins_round(attacker, defender, p1, p2)


    def reorder(self):
        """This method is to reorder the list after a round of battle takes place. Since the value of the criterion will
         be changing, either increasing or decreasing. The pokemon order will need to be updated. This method does this
         by removing the pokemon who just battled, based on the attribute, and then appending it back to the list. The
         list adt will then take the new value and place it in the appropriate place based on the key value.
         best and worst case is O(nlogn) where n is the size of the list
        """

        for i in range(len(self.team1)):
            if self.team1[i].value.get_hp() <= 0:
                self.team1.delete_at_index(i)

        if self.criterion1 =='hp':

            if self.criterion2=='hp':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_hp())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_hp())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.
                

            elif self.criterion2 == 'lvl':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_hp())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, self.team1[0].key - self.team2[0].value.get_level())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.


            elif self.criterion2 == 'Attack':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_hp())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_attack_damage())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'Defence':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_hp())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_defence())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'Speed':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_hp())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_defence())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

        elif self.criterion1 == 'lvl':

            if self.criterion2 == 'lvl':
                new_item = ListItem(self.team1[0].value, self.team1[0].key - self.team1[0].value.get_level())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, self.team1[0].key - self.team2[0].value.get_level())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'hp':
                new_item = ListItem(self.team1[0].value, self.team1[0].key - self.team1[0].value.get_level())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_hp())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'Attack':
                new_item = ListItem(self.team1[0].value, self.team1[0].key - self.team1[0].value.get_level())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_attack_damage())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'Defence':
                new_item = ListItem(self.team1[0].value, self.team1[0].key - self.team1[0].value.get_level())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_defence())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'Speed':
                new_item = ListItem(self.team1[0].value, self.team1[0].key - self.team1[0].value.get_level())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_speed())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

        elif self.criterion1 == 'Attack':
            if self.criterion2 == 'lvl':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_attack_damage())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, self.team1[0].key - self.team2[0].value.get_level())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'hp':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_attack_damage())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_hp())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'Attack':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_attack_damage())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_attack_damage())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'Defence':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_attack_damage())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_defence())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'Speed':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_attack_damage())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_speed())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

        elif self.criterion1 == 'Defence':

            if self.criterion2 == 'lvl':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_defence())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, self.team1[0].key - self.team2[0].value.get_level())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'hp':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_defence())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_hp())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'Attack':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_defence())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_attack_damage())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'Defence':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_defence())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_defence())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'Speed':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_defence())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_speed())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

        elif self.criterion1 == 'Speed':
            if self.criterion2 == 'lvl':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_speed())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, self.team1[0].key - self.team2[0].value.get_level())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'hp':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_speed())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_hp())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'Attack':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_speed())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_attack_damage())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'Defence':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_speed())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_defence())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.

            elif self.criterion2 == 'Speed':
                new_item = ListItem(self.team1[0].value, 0 - self.team1[0].value.get_speed())  # copy the pokemon
                self.team1.delete_at_index(0)
                self.team1.add(new_item)  # reorder the list by delete the changing data, and add it.

                new_item1 = ListItem(self.team2[0].value, 0 - self.team2[0].value.get_speed())  # copy the pokemon
                self.team2.delete_at_index(0)
                self.team2.add(new_item1)  # reorder the list by delete the changing data, and add it.



    def both_alive(self) -> None:
        """If both of the pokemon are alive, they will be moved from the first place of the team,
        then send back to the team (reorder) for battle mode =1 or battle mode=2. If battle mode is 0, they will return back to their teams by being pushed onto the stack.
        Complexity O(1) for best case when battle mode =0
        O(nlogn) for worst where n is the size of the list and battle mode =2"""
        if self.battle_mode == 2:
            self.reorder()
        elif self.battle_mode == 1:
            self.team1.append(self.poke1)
            self.team2.append(self.poke2)
        else:
            self.team1.push(self.poke1)
            self.team2.push(self.poke2)

    def level_up(self)-> None:
        """Increases the level of a pokemon. If the level is changed, the order of the pokemon should be changed.
        Complexity O(1) for both cases"""
        self.set_level(self.get_level() + 1)

    def reduce_hp(self) -> int:
        """Reduces the hp of a pokemon by 1
        Complexity O(1) for both cases """
        return self.get_hp() - 1

    def simultaneous_battle(self, p1, p2) -> None:
        """ When the speeds of two pokemon are equal, they will both attack and
        defend simultaneously.
        Complexity, O(1) for best when battle mode=0, O(nlogn) for worst, where n is the size of the list and battle mode =2.
        """

        #both attack each other
        #calculates the hp of p1 after being attacked by p2
        p1_hp = p1.get_damage_after_attack(p2.get_poke_type(), p2.get_level())

        #if hp is less than 0, set it to 0, since non negative hps are not allowed
        if p1_hp < 0:
            p1.set_hp(0)
            p1_hp = 0
        else:
            #if hp is above zero, set the hp to the new value
            p1.set_hp(p1_hp)

        #calculates the hp of p1 after being attacked by p2
        p2_hp = p2.get_damage_after_attack(p1.get_poke_type(), p1.get_level())

        #if hp is less than 0, set it to 0, since non negative hps are not allowed
        if p2_hp < 0:
            p2.set_hp(0)
            p2_hp = 0
        else:
            #if hp is above zero, set the hp to the new value
            p2.set_hp(p2_hp)

        #if p1 faints and p2 is still alive, p2 gains a level and p1 is removed from the team
        if p2_hp>0 and p1_hp==0:
            self.p2_wins_round(p1,p2)

        # if p2 faints and p1 is still alive, p1 gains a level and p2 is removed from the team
        elif p1_hp > 0 and p2_hp == 0:
            self.p1_wins_round(p1,p2)
        # if neither faints, both hps will be reduced by 1
        elif p1_hp > 0 and p2_hp > 0:
            p1_hp = Battle.reduce_hp(p1)
            p2_hp = Battle.reduce_hp(p2)
            p1.set_hp(p1_hp)
            p2.set_hp(p2_hp)

            # if p1 faints and p2 is still alive, p2 gains a level and p1 is removed from the team
            if p1_hp == 0 and p2_hp > 0:
                self.p2_wins_round(p1, p2)
            elif p1_hp > 0 and p2_hp == 0:
                self.p1_wins_round(p1, p2)
            elif p1_hp == 0 and p2_hp == 0:
                self.both_faint()
            else:
                self.both_alive()

        else:
            #both fainted and need to be removed
            self.both_faint()


    def set_pokemon(self) -> int:
        """Sets the values of c,b,s according to the amount of each pokemon entered by the user.
        These values will then be passed into assign_team where the team will be created according to the number of each pokemon.
        Complexity O(1) for best and worst case"""
        c,b,s=PokeTeam.team[0],PokeTeam.team[1],PokeTeam.team[2] #assigns the user input for the team to c, b and s
        return c,b,s

    def team_is_empty(self, team1, team2) -> bool:
        """Checks if either team is empty and then returns True if at least one team is
        Complexity is O(1) for best and worst"""
        if self.team1.is_empty() or self.team2.is_empty():
            return True

    def battle_result(self, team1, team2) -> str:
        """Determines the result of the battle and returns the result, assigns the remaining pokemon to the team, so it can be printed.
        Complexity isO(n) for best case when battle mode=0, O(nlogn) for worst when battle mode =2"""


        # if team1 is empty and team2 not is empty, then trainer two has won
        if self.team1.is_empty() and not self.team2.is_empty():
            self.team2=self.remaining_pokemon(self.team2)
            return self.trainer_two_name

        # if team2 is empty and team1 is not empty, then trainer one has won
        elif self.team2.is_empty() and not self.team1.is_empty():
            self.team1=self.remaining_pokemon(self.team1)
            return self.trainer_one_name

        # if both teams are empty then it is a draw
        else:
            if not self.team1.is_empty() and not self.team2.is_empty():
                self.team1=self.remaining_pokemon(self.team1)
                self.team2=self.remaining_pokemon(self.team2)
            else:
                self.team1=None
                self.team2=None
            return "Draw"

    def set_attacker_defender(self, p1, p2) -> None:
        """Sets the attacker pokemon and the defender pokemon depending on the speed. If the speeds are equal, pokemon will fight a simultaneous battle
        Complexity O(1) is the best when battle mode= 0 and O(nlogn) for worst case when battle mode = 2"""
        # if the pokemon in team1 has a higher speed than the pokemon in team2, it will be the attacker and p2 will be the defender
        if p1.get_speed() > p2.get_speed():
            attacker = p1
            defender = p2
            self.battle_attacker_defender(attacker, defender, p1, p2)

        # if the pokemon in team2 has a higher speed than the pokemon in team1, it will be the attacker and p1 will be the defender
        elif p1.get_speed() < p2.get_speed():
            attacker = p2
            defender = p1

            self.battle_attacker_defender(attacker, defender, p1, p2)

        # if both pokemon have the same speed, they will attack and defend simultaneoulsy
        else:
            self.simultaneous_battle(p1, p2)


    def attacker_wins_round(self, attacker, defender, p1, p2)-> None:
        """When the attacker causes the defender to faint, the attacker wins the round and needs to gain one level.
        Since the defender fainted, it also needs to be removed from its team. (for mode 2, winner also need to remove,
        but need to be added back in order to reorder.)
        Also, this function checks if attacker is the GlitchMon and if yes then it increases it's stats
        Complexity O(1) for best
        O(n) for worst
        """
        Battle.level_up(attacker)
        if attacker == GlitchMon :
            self.stat_increase()
        if self.battle_mode==0:
            if defender == p2:
                self.team1.push(self.poke1) #return back to team
            else:
                self.team2.push(self.poke2)#return back to team
        elif self.battle_mode==1:
            if defender==p2:
                self.team1.append(self.poke1)
            else:
                self.team2.append(self.poke2)
        else:
            if defender==p2:
                self.reorder()
                self.team2.delete_at_index(0)  # delete the fainted pokemon
            else:
                self.team1.delete_at_index(0)  # delete the pokemon who is fainted.
                self.reorder()

    def defender_wins_round(self, attacker, defender, p1, p2)-> None:
        """When the defender causes the attacker to faint, the defender wins the round and needs to gain one level.
        Since the attacker fainted, it also needs to be removed from its team.(for mode 2, winner also need to remove,
        but need to be added back in order to reorder.)
        Also, this function checks if defender is the GlitchMon and if yes then it increases it's stats
        Complexity O(1) for best where the battle mode =1 
        O(nlogn) for worst, where n is the size of the list and the battle mode =2
        """

        Battle.level_up(defender)
        # for increasing the stats of the GlitchMon if it wins
        if defender == GlitchMon :
            self.stat_increase()
        if self.battle_mode==0:
            if attacker == p2:
                self.team1.push(self.poke1) #return back to team
            else:
                self.team2.push(self.poke2) #return back to team
        elif self.battle_mode==1:
            if attacker==p2:
                self.team1.append(self.poke1)
            else:
                self.team2.append(self.poke2)
        else:
            if attacker==p2:
                newitem = ListItem(self.team1[0].value, self.team1[0].key)  # copy the pokemon
                self.reorder()
                self.team2.delete_at_index(0)  # delete the fainted pokemon
            else:
                self.team1.delete_at_index(0)  # delete the pokemon who is fainted.
                self.reorder()

    def both_faint(self)-> None:
        """In the case where both pokemon faint, they both need to be removed from their teams.
        Complexity O(n) for best and worst case
        """
        # they have both fainted, remove from team
        if self.battle_mode == 2:
            self.team2.delete_at_index(0)
            self.team1.delete_at_index(0)



    def set_defender_hp(self, defender, p1, p2, defender_hp)-> None:
        """This is a method to update the defenders hp when it changes, by changing it via the instance so it is accessible.
        Complexity O(1) for best and worst.
        """
        defender.set_hp(defender_hp)
        if defender == p1:
            p1.set_hp(defender_hp)
        else:
            p2.set_hp(defender_hp)

    def set_attacker_hp(self, attacker, p1, p2, attacker_hp) -> None:
        """This is a method to update the attackers hp when it changes, by changing it via the instance so it is accessible.
        Complexity O(1) for best and worst.
        """
        attacker.set_hp(attacker_hp)
        if attacker == p1:
            p1.set_hp(attacker_hp)
        else:
            p2.set_hp(attacker_hp)

    def rotating_mode_battle(self) -> str:
        """Sets the battle mode as 1, and creates two teams based off user input which then battle until
        at least one team is empty, and then declares the winner. The pokemon return to the end of their teams after they perform one battle.
        :pre: teams cannot be empty before battle has started
        :raises: ValueError when at least one of teams is empty before battle
        :complexity: O(1) for best case, where there is only one pokemon in one team, and that pokemon faints, this means
        the while loop will only execute once
        O(n) for worst case, where n is the number of pokemon in the highest of the two teams, where the while loop will iterate
        and each pokemon will have to faint until the team is empty"""
        
        self.battle_mode = 1
        
        # creating the first team
        PokeTeam.choose_team(PokeTeam, 1)
        c, b, s = Battle.set_pokemon(self)
        self.team1 = PokeTeam.assign_team(self,c, b, s)

        # creating the second team
        PokeTeam.choose_team(PokeTeam, 1)
        c, b, s = Battle.set_pokemon(self)
        self.team2 = PokeTeam.assign_team(self,c, b, s)


        #while not self.team_is_empty(self.team1,self.team2):
        if self.team_is_empty(self.team1,self.team2):
            raise ValueError("team can not be empty before battle")
        
        #loop until at least on team is empty
        while not self.team_is_empty(self.team1, self.team2):
            poke1 = self.team1.serve() #poke1 contains first pokemon in queue (team1)
            self.poke1=poke1

            poke2 = self.team2.serve() #poke2 contains first pokemon in queue (team2)
            self.poke2=poke2

            p1=poke1
            p2=poke2

            self.set_attacker_defender(p1, p2)

        return self.battle_result(self.team1, self.team2)

    def optimised_mode_battle(self, criterion_team1: str, criterion_team2: str) -> str:
        """ This method is a battle mode that the user gets to choose an attribute to order their team. They can choose
        between Level, HP, Attack, Defence and Speed. This order will be maintained throughout the battle, even when the
        stats change after each round. The battle mode for this mode is 2.

        pre: teams cannot be empty before battle has started
        raises: ValueError when at least one of teams is empty before battle
        complexity: O(1) for best case, where there is only one pokemon in one team, and that pokemon faints, this means
        the while loop will only execute once
        O(nlogn) for worst case, where n is the number of pokemon in the highest of the two teams, where the while loop will iterate
        and each pokemon will have to faint until the team is empty
        """
        self.battle_mode = 2
        self.criterion1 = criterion_team1
        self.criterion2 = criterion_team2

        # creating the first team
        PokeTeam.choose_team(PokeTeam, 2)
        c, b, s = Battle.set_pokemon(self)
        self.criterion=self.criterion1

        self.team1 = PokeTeam.assign_team(self, c, b, s)

        # creating the second team

        PokeTeam.choose_team(PokeTeam, 2)
        c, b, s = Battle.set_pokemon(self)
        self.criterion=self.criterion2

        self.team2 = PokeTeam.assign_team(self, c, b, s)


        # if one of the teams is empty before the battle, raise a ValueError
        if self.team_is_empty(self.team1, self.team2):
            raise ValueError("Teams cannot be empty before battle")

        #initializing some variables which will be used in the loop
        poke1=''
        poke2=''


        while not self.team_is_empty(self.team1,self.team2):

            # pokemon battling in team 1 are [0] and are assigned to poke1, only if poke1 does not already contain the pokemon
            if poke1!=self.team1[0].value:
                poke1=self.team1[0].value
                p1 = poke1  # creates an instance of poke1

            # pokemon battling in team 2 are [0] and are assigned to poke2, only if poke2 does not already contain the pokemon
            if poke2!=self.team2[0].value:
                poke2=self.team2[0].value
                p2 = poke2 #creates an instance of poke2

            #method to decide which pokemon is an attacker and which is a defender
            self.set_attacker_defender(p1,p2)
        #self.final_battle()
        #call battle result to determine who won or if the battle resulted in a draw
        return self.battle_result(self.team1,self.team2)


    def p1_wins_round(self,p1,p2):
        """In the case of a simultaneous battle, if p1 wins the round, then p1 will level up and p2 will be removed from the team.
        Also, this function checks if p1 is the GlitchMon and if yes then it increases it's stats
        Complexity: O(1) for best when battle mode =0, and O(n) for worst, when battle mode =2"""
        Battle.level_up(p1)
        if p1==GlitchMon:
            self.stat_increase()

        if self.battle_mode==0:
            self.team1.push(p1)
        elif self.battle_mode==1:
            self.team1.append(self.poke1)
        else:
            self.team2.delete_at_index(0)


    def p2_wins_round(self,p1,p2):
        """In the case of a simultaneous battle, if p2 wins the round, then p2 will level up and p1 will be removed from the team.
        Also, this function checks if p1 is the GlitchMon and if yes then it increases it's stats
        Complexity: O(1) for best when battle mode =0, and O(n) for worst, when battle mode =2"""
        # if p1 faints and p2 is still alive, p2 gains a level and p1 is removed from the team
        Battle.level_up(p2)
        # for increasing the stats of the GlitchMon if it wins
        if p2 == GlitchMon:
            self.stat_increase()
        Battle.level_up(p2)
        if self.battle_mode == 0:
            self.team2.push(p2)
        elif self.battle_mode == 1:
            self.team1.append(self.poke2)
        else:
            self.team1.delete_at_index(0)
            

    def remaining_pokemon(self,team):
        """Creates a string with the updated stats of the remaining pokemon after a battle has ended
        Complexity: Best case, O(1), when both teams are empty and worst case is O(n) where n is the size of the team, and battle mode =2"""

        return_str=""
        if self.battle_mode==0:
            while not team.is_empty():
                return_str += str(team.pop()) + ", "
            return_str = return_str[:-2]
            return return_str

        elif self.battle_mode==1:
            while not team.is_empty():
                return_str += str(team.serve()) + ", "
            return_str = return_str[:-2]
            return return_str

        else:
            #sort team one more time before returning
            team=self.final_sort(team)
            while not team.is_empty():
                return_str += str(team[0].value) + ", "
                team.delete_at_index(0)
            return_str = return_str[:-2]
            return return_str
        
    def final_sort(self,arr):
        """
        This function depedning on if the arr is team1 or 2, we set the criterion to the corresponding teams. Then we sort the team once more so it is in the correct order and return it.
        Cmplexity O(nlogn) for best and worst, where n is the size of the list.
        """  
        #depedning on if the arr is team1 or 2, we set the criterion to the corresponding teams'
        if arr==self.team1:
            crit=self.criterion1
        else:
            crit=self.criterion2
        if crit=='hp':
            new_item = ListItem(arr[0].value, 0 - arr[0].value.get_hp())  # copy the pokemon
            arr.delete_at_index(0)
            arr.add(new_item)  # reorder the list by delete the changing data, and add it.
        elif crit=='lvl':
            new_item = ListItem(arr[0].value, 0 - arr[0].value.get_level())  # copy the pokemon
            arr.delete_at_index(0)
            arr.add(new_item)  # reorder the list by delete the changing data, and add it.
        elif crit=='attack':
            new_item = ListItem(arr[0].value, 0 - arr[0].value.get_attack_damage())  # copy the pokemon
            arr.delete_at_index(0)
            arr.add(new_item)  # reorder the list by delete the changing data, and add it.
        elif crit == 'defence':
            new_item = ListItem(arr[0].value, 0 - arr[0].value.get_defence())  # copy the pokemon
            arr.delete_at_index(0)
            arr.add(new_item)  # reorder the list by delete the changing data, and add it.
        else:
            new_item = ListItem(arr[0].value, 0 - arr[0].value.get_speed())  # copy the pokemon
            arr.delete_at_index(0)
            arr.add(new_item)  # reorder the list by delete the changing data, and add it.

        length = len(arr)
 
        j = 0

        #iterate until j= the size of the loop
        while j < length - 1:
 
            # Checking the conditions, where the previous key should be more than the one after it
            if (arr[j].key > arr[j + 1].key):

                #if the above condition is true we need to swap
                temp = arr[j].key
                arr[j].key = arr[j + 1].key
                arr[j + 1].key = temp

                #updating value of j
                j = -1
            j += 1

        return arr

    def final_battle(self):
        """        
        This method should be added before the into the method of set_mode_battle/ rotating_mode_battle/ optimised_mode_battle,
        after the team is empty, it will add the new pokemon to the team. Then it will send the new team to fight until
        the team is empty.
        best case: O(1)
        worst case: O(n)
        """
        if self.team1.is_empty() and PokeTeam.team[-1] == 1:
            if self.battle_mode == 0:
                self.team1.push(GlitchMon())
                p1 = self.team1.peak()

            elif self.battle_mode == 1:
                self.team1.append(GlitchMon())
                p1 = self.team1.serve()

            elif self.battle_mode == 2:
                self.team1.add2(ListItem(GlitchMon(), 0))
                self.team1.add2
                p1 = self.team1[0].value

        elif self.team2.is_empty() and PokeTeam.team[-1] == 1:
            if self.battle_mode == 0:
                self.team2.push(GlitchMon())
                p2 = self.team2.peak()
            elif self.battle_mode == 1:
                self.team2.append(GlitchMon())
                p2 = self.team2.serve()
            elif self.battle_mode == 2:
                self.team2.add2(ListItem(GlitchMon(), 0))
                self.team2.add2
                p2 = self.team2[0].value

        while not self.team_is_empty(self.team1, self.team2):
            if self.battle_mode == 0:
                self.set_attacker_defender(self.team1.peak(), self.team2.peak())
            elif self.battle_mode == 1:
                self.set_attacker_defender(self.team1.serve(), self.team2.serve())
            elif self.battle_mode == 2:
                self.set_attacker_defender(self.team1[0].value, self.team2[0].value)




#x=Battle("Cynthia","Steven")
#print(x.optimised_mode_battle("hp", "lvl"))
#print(x.team1)
#print(x.rotating_mode_battle())

