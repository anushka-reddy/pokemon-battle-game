"""
PokeTeam Class, inheriting from the Pokemon Child Classes and the ArrayR and ArrayStack Class. This class enables the creation of the team.
"""

__author__ = "Nabiha Siddiqui, Aiguo Shao, Yuheng Shang, Anushka Reddy"

from queue_adt import CircularQueue
from array_sorted_list import ArraySortedList
from pokemon import Squirtle
from pokemon import Charmander
from pokemon import Bulbasaur
from stack_adt import Stack
from referential_array import ArrayR, T
from stack_adt import ArrayStack
from sorted_list import ListItem

class PokeTeam():
    """Class representing the Pokemon Team chosen by the user"""
    def __init__(self, trainer_name):
        """Initialization"""
        self.trainer_name= trainer_name
        self.battle_mode=0
        self.team=None
        Charmander.__init__(self)
        Bulbasaur.__init__(self)
        Squirtle.__init__(self)


    def team_is_allowed(self) -> bool:
        """Returns boolean value, value depends on if the team size is less than or equal to 6.
        Complexity: O(1) for best case, where there would only be one pokemon in the team.
        the worst case would be O(n), where n is the size of the list 'team'.
        """
        return sum(self.team) <= 6

    def choose_team(self, battle_mode: int, criterion: str = None) -> None:
        """Takes a user input for the number of each pokemon in their team, and continuously asks for user input until the size of their team is less than or equal to 6.
        and their input must be in the correct format x x x.
        Pre: Only numbers entered, 3 values entered and the sum of those three values must be less than or equal to 6
        Raises: ValueError if integer is not input
        Complexity: Best case is O(n) where n is the amount of characters a user has input. Worst Case is
        """
        string_space = "      "
        #loop iterates until valid team is input
        while True:
            self.team = []
            num_pokemon = input("Howdy Trainer! Choose your team as C B S \nwhere C is the number of Charmanders\n" +
                                string_space + "B is the number of Bulbasaurs\n" + string_space + "S is the number of "
                                                                                                  "Squirtles\n> ")
            for x in num_pokemon.split(' '):
                try:
                    int(x) #see if x is an integer
                except ValueError: #if not code raises a value error
                        print("Please enter numbers only")
                        break
                else:
                    self.team += [int(x)]
            if sum(self.team)<=6 and len(self.team)==3: #if the size of the team is equal to 3 and the total amount of pokemon in the team is less than or equal to 6, then we can exit the loop
                break
            else: #if not, print prompt
                print("Please enter a valid team, where the team may only contain 6 or less pokemon. The input must also be in the format x x x")



    def assign_team(self, charm: int, bulb: int, squir: int):
        """ if battle mode is 0, it creates a stack containing the correct order of the pokemon, where charmanders will
        be at the top of the stack, then bulbasaurs, then squirtles. This uses two stacks, the first stack creates the
        reverse order where charamanders are at the bottom of the stack, then by popping off and appending the pokemon
        of that stack into a new stack, we have the correct order.

        If the battle mode is 1, it creates a queue, containting the correct order of the pokemon, where charmanders will
        be in the front, then bulbasaurs, then squirtles. 

        If battle mode is 2, it will create a list, and the order of pokemon will be in a non-increasing order based on the attribute chosen by the user i.e. Level, HP,
        Attack, Defence and speed. We implemented the list to be sorted in non-increasing order by creating the attributes to be their negatives i.e. spped of 7 would become -7
        that way the largest value of an attribute would be in the front of the list because it would be the 'smallest' when negative. Additonally, when the values of the attributes
        were equivalent then we would use the same implementation of add from sorted array, except that we changed < or > to <= or >=, that way the pokemon would be appended to the list
        where the first pokemon appended would remain in the front of the list. 

        Complexity: Best Case is O(1), is when there is only one type of pokemon and only one of that pokemon, in a team.
        Worst Case is O(n*m)logn, where n is the size of the team the user had input and m is the count (where the count is the count of each type of pokemon).
        """
        if self.battle_mode == 0:  # check if the battle_mode=0
            lst = [charm, bulb, squir]
            my_stack = ArrayStack(sum(lst)) #creates the stack adt
            for x in range(len(lst) - 1, -1, -1): #iterating through the lst backwards, since the stack is FILO
                count = 0
                if lst[x] > 0: # if there is at least one pokemon, of a certain type
                    while lst[x] != count:  # loop until we have dealt with all pokemon of that type
                        # appends the appropriate instance by checking the index, to the stack
                        if x == 0:
                            my_stack.push(Charmander())
                        elif x == 1:
                            my_stack.push(Bulbasaur())
                        else:
                            my_stack.push(Squirtle())
                        count +=1
            return my_stack

        elif self.battle_mode == 1:  # check if the battle_mode=1
            lst = [charm, bulb, squir]  # this is the order the pokemon come out in
            my_queue = CircularQueue(sum(lst))  # create a queue of the appropriate size
            string_name = ""
            # iterate through the user input, charm, bulb and squir
            count = 0
            for x in range(len(lst)):
                count = 0
                if lst[x] > 0: # if there is at least one pokemon, of a certain type
                    while lst[x] != count:  # loop until we have dealt with all pokemon of that type
                        # appends the appropriate instance by checking the index, to the queue
                        if x == 0:
                            my_queue.append(Charmander())
                        elif x == 1:
                            my_queue.append(Bulbasaur())
                        else:
                            my_queue.append(Squirtle())
                        count +=1
            return my_queue


        elif self.battle_mode == 2:
                #check if the battle_mode=2
            if self.criterion == "lvl":  # if choose the Level as attribute
                lst = [charm, bulb, squir]   #have the correct order base on the attribute, this order is default in case the attributes are equivalent
                my_list = ArraySortedList(sum(lst))  # create a list that has the length of the sum of pokemon
                for x in range(0, len(lst)):
                    count = 0
                    if lst[x] > 0:  # if there is at least one pokemon
                        while lst[x] != count:
                            # add object to the list in this order
                            if x == 0:
                                my_list.add2(ListItem(Charmander(), 0 - Charmander.get_level(Charmander)))
                            elif x== 1:
                                my_list.add2(ListItem(Bulbasaur(), 0 - Bulbasaur.get_level(Bulbasaur)))
                            else:
                                my_list.add2(ListItem(Squirtle(), 0 - Squirtle.get_level(Squirtle)))
                            count +=1

            elif self.criterion == "hp":     # if choose the HP as attribute
                lst = [bulb, squir, charm]  # have the correct order base on the default attribute
                my_list = ArraySortedList(sum(lst))  # create a list that has the length of the sum of pokemon
                if Bulbasaur.get_hp(Bulbasaur) == Squirtle.get_hp(Squirtle) == Charmander.get_hp(Charmander):
                    lst = [charm, bulb, squir]  # in the case where the hps are equal, this is the default order the pokemon need to be in
                    for x in range(0, len(lst)):
                        count = 0
                        if lst[x] > 0:  # if there is at least one pokemon
                            while lst[x] != count:
                                #add object to the list in this order
                                if x == 0:
                                    my_list.add2(ListItem(Charmander(), 0 - Charmander.get_hp(Charmander)))
                                elif x == 1:
                                    my_list.add2(ListItem(Bulbasaur(), 0 - Bulbasaur.get_hp(Bulbasaur)))
                                else:
                                    my_list.add2(ListItem(Squirtle(), 0 - Squirtle.get_hp(Squirtle)))
                                count +=1

                else:
                    #if attribute values are different
                    for x in range(0, len(lst)):
                        count = 0
                        if lst[x] > 0:  # if there is at least one pokemon
                            while lst[x] != count:
                                #add object to the list in this order
                                if x == 0:
                                    my_list.add(ListItem(Bulbasaur(), 0 - Bulbasaur.get_hp(Bulbasaur)))
                                elif x == 1:
                                    my_list.add(ListItem(Squirtle(), 0 - Squirtle.get_hp(Squirtle)))
                                else:
                                    my_list.add(ListItem(Charmander(), 0 - Charmander.get_hp(Charmander)))
                                count +=1

            elif self.criterion == "Attack":     # if choose the attack as attribute
                lst = [charm, bulb, squir]  # have the correct order base on the attribute
                my_list = ArraySortedList(sum(lst))  # create a list that has the length of the sum of pokemon
                if Bulbasaur.get_attack_damage(Bulbasaur) == Squirtle.get_attack_damage(Squirtle) == Charmander.get_attack_damage(Charmander): #if attributes values are equal, will be using add2 method to add objects to the list

                    for x in range(0, len(lst)):
                        count = 0
                        if lst[x] > 0:  # if there is at least one pokemon
                            while lst[x] != count:
                                #add object to the list in this order
                                if x == 0:
                                    my_list.add2(ListItem(Charmander(), 0 - Charmander.get_attack_damage(Charmander)))
                                elif x == 1:
                                    my_list.add2(ListItem(Bulbasaur(), 0 - Bulbasaur.get_attack_damage(Bulbasaur)))
                                else:
                                    my_list.add2(ListItem(Squirtle(), 0 - Squirtle.get_attack_damage(Squirtle)))
                                count += 1

                else:
                    lst = [bulb, squir, charm]  # have the correct order base on the default values of attributes
                    for x in range(0, len(lst)):
                        count = 0
                        if lst[x] > 0:  # if there is at least one pokemon
                            while lst[x] != count:
                                #add object to the list in this order
                                if x == 0:
                                    my_list.add(ListItem(Bulbasaur(), 0 - Bulbasaur.get_attack_damage(Bulbasaur)))
                                elif x == 1:
                                    my_list.add(ListItem(Squirtle(), 0 - Squirtle.get_attack_damage(Squirtle)))
                                else:
                                    my_list.add(ListItem(Charmander(), 0 - Charmander.get_attack_damage(Charmander)))
                                count += 1
            elif self.criterion == "Defence":     # if choose the Defence as attribute
                lst = [charm, bulb, squir]  # have the correct order base on the attribute
                my_list = ArraySortedList(sum(lst))  # create a list that has the length of the sum of pokemon
                if Bulbasaur.get_defence(Bulbasaur) == Squirtle.get_defence(Squirtle) == Charmander.get_defence(Charmander):
                    for x in range(0, len(lst)):
                        count = 0
                        if lst[x] > 0:  # if there is at least one pokemon
                            while lst[x] != count:
                                #add object to the list in this order
                                if x == 0:
                                    my_list.add2(ListItem(Charmander(), 0 - Charmander.get_defence(Charmander)))
                                elif x == 1:
                                    my_list.add2(ListItem(Bulbasaur(), 0 - Bulbasaur.get_defence(Bulbasaur)))
                                else:
                                    my_list.add2(ListItem(Squirtle(), 0 - Squirtle.get_defence(Squirtle)))
                                count += 1

                    else:
                        lst = [bulb, squir, charm]#have the correct order base on the default attributes
                        for x in range(0, len(lst)):
                            count = 0
                            if lst[x] > 0:  # if there is at least one pokemon
                                while lst[x] != count:
                                    # add object to the list in this order
                                    if x == 0:
                                        my_list.add(ListItem(Bulbasaur(), 0 - Bulbasaur.get_defence(Bulbasaur)))
                                    elif x == 1:
                                        my_list.add(ListItem(Squirtle(), 0 - Squirtle.get_defence(Squirtle)))
                                    else:
                                        my_list.add(ListItem(Charmander(), 0 - Charmander.get_defence(Charmander)))
                                    count += 1

            elif self.criterion == "Speed":     # if choose the Speed as attribute
                lst = [charm, bulb, squir]  # have the correct order base on the attribute
                my_list = ArraySortedList(sum(lst))  # create a list that has the length of the sum of pokemon
                if Bulbasaur.get_speed(Bulbasaur) == Squirtle.get_speed(Squirtle) == Charmander.get_speed(Charmander):
                    for x in range(0, len(lst)):
                        count = 0
                        if lst[x] > 0:  # if there is at least one pokemon
                            while lst[x] != count:
                                #add object to the list in this order
                                if x == 0:
                                    my_list.add2(ListItem(Charmander(), 0 - Charmander.get_speed(Charmander)))
                                elif x == 1:
                                    my_list.add2(ListItem(Bulbasaur(), 0 - Bulbasaur.get_speed(Bulbasaur)))
                                else:
                                    my_list.add2(ListItem(Squirtle(), 0 - Squirtle.get_speed(Squirtle)))
                                count += 1

                    else:
                        lst = [bulb, squir, charm]  # have the correct order base on the attribute
                        for x in range(0, len(lst)):
                            count = 0
                            if lst[x] > 0:  # if there is at least one pokemon
                                while lst[x] != count:
                                    # add object to the list in this order
                                    if x == 0:
                                        my_list.add(ListItem(Bulbasaur(), 0 - Bulbasaur.get_speed(Bulbasaur)))
                                    elif x == 1:
                                        my_list.add(ListItem(Squirtle(), 0 - Squirtle.get_speed(Squirtle)))
                                    else:
                                        my_list.add(ListItem(Charmander(), 0 - Charmander.get_speed(Charmander)))
                                    count += 1
            return my_list


    def set_battle_mode(self, battle_mode) -> None:
        """Sets the battle mode according to the user input
        :pre: battle mode must be either 0, 1 or 2
        :raises: ValueError when battle mode is not 0, 1 or 2
        :complexity: O(1) for best and worst case"""
        valid_battle_modes=[0,1,2] #valid battle modes are in this list
        if battle_mode not in valid_battle_modes:
           raise ValueError("Battle mode must be either 0, 1 or 2") #if the user input battle mode is not in the list, program will raise a ValueError
        else:
            self.battle_mode = battle_mode #if it is a valid battle mode, it will be saved as an instance varibale

    def __str__(self) -> str:
        """Prints the team
        Complexity: O(n) for best and worst case
        """
        return_str = ""
        for x in range(len(self.team)): #iterating through the entire team
            if self.team[x] > 0: #if the value at the index x is more than 0, this means pokemon are present
                if x == 0: #if the index value is 0, we need to return charamanders info
                    return_str += Charmander.poke_name + "'s HP = " + str(
                        Charmander.get_hp(Charmander)) + " and " + "level = " + str(Charmander.get_level(Charmander))
                elif x == 1: #if the index is 1, then we need to return bulbasaurs info
                    return_str += ", "
                    return_str += Bulbasaur.poke_name + "'s HP = " + str(
                        Bulbasaur.get_hp(Bulbasaur)) + " and " + "level = " + str(Bulbasaur.get_level(Bulbasaur))
                elif x == 2: #if the index is 2, then we need to return squirtle's info
                    return_str += ", "
                    return_str += Squirtle.poke_name + "'s HP = " + str(
                        Squirtle.get_hp(Squirtle)) + " and " + "level = " + str(Squirtle.get_level(Squirtle))
        return return_str




