"""
Child Classes of different Pokemon including the glitch pokemon, inheriting from the base class PokemonBase. Child classes override the abstract methods and enable access to Pokemon Specific Characteristics
"""

__author__ = "Nabiha Siddiqui, Anushka Reddy"

import random

from pokemon_base import PokemonBase

valid_types = ["fire", "water", "grass"]


class Squirtle(PokemonBase):
    """ Class representing a Squirtle """

    # initilaizing the class variables, these values are all the necessary pokemon stats (not including the operations that are required to be performed for some stats, this will be done in the methods)
    poke_name = "Squirtle"
    poke_type = "water"
    hp = 8
    poke_attack = 4
    poke_defence = 6
    poke_speed = 7

    def __init__(self) -> None:
        """Initialization"""
        PokemonBase.__init__(self, Squirtle.hp, Squirtle.poke_type)

    def pokemon_name(self) -> str:
        """Returns the name of the Pokemon
        Complexity: O(1) for best and worst"""
        return self.poke_name

    def get_speed(self) -> int:
        """Returns the speed of the pokemon
        Complexity: O(1) for best and worst"""
        return self.poke_speed

    def get_attack_damage(self) -> int:
        """Returns the attack damage
        Complexity: O(1) for best and worst"""
        return 4 + self.level // 2

    def get_poke_type(self) -> str:
        """Returns the poke_type
        Complexity: O(1) for best and worst"""
        return self.poke_type

    def set_level(self, level) -> None:
        """Allows the user to set the level of Squirtle
        Pre: Level must be more than or equal to 1
        raises: ValueError if level is not more than or equal to 1
        Complexity: O(1) for best and worst"""
        if level <= 0:
            raise ValueError("Level must be more than or equal to 1")
        else:
            self.level = level

    def get_level(self) -> int:
        """Returns the level
        Complexity: O(1) for best and worst"""
        return self.level

    def set_hp(self, hp) -> None:
        """Allows the user to set the HP
        Pre: HP must be more than 0
        raises: ValueError when HP<0
        Complexity: O(1) for best and worst"""
        if hp < 0:
            raise ValueError("HP cannot be less than 0")
        else:
            self.hp = hp

    def get_hp(self) -> int:
        """Returns the HP
        Complexity: O(1) for best and worst"""
        return self.hp

    def cal_effective_damage(self, poke_type, level) -> int:
        """Calculates the effective damage squirtle will take when battling a pokemon
        of a certain type. The user will enter the opponent's poke_type and level, and
        depending on these values the program will calculate the damage Squirtle will endure.
        Pre: poke_type entered must be either 'water','fire', or 'grass'
        Pre: level must be more than or equal to 1
        raises: value error when either condition is violated
        Complexity: O(1) for best and worst case"""
        valid_types = ["fire", "water", "grass"]
        if level <= 0:
            raise ValueError("level must be more than or equal to 1")
        elif poke_type not in valid_types:
            raise ValueError("Pokemon must be of type 'fire','water' or 'grass'")
        else:
            if poke_type == 'fire':
                return 0.5 * (6 + level)
            elif poke_type == 'water':
                return 1 * (4 + level // 2)
            elif poke_type == 'grass':
                return 2 * (5)

    def get_damage_after_attack(self, poke_type, level) -> int:
        """Calculates the hp of Squirtle when being attacked by a pokemon
        of a certain type and level. The user will enter the opponent's poke_type and level, and
        depending on these values the program will calculate the hp of the resultant attack.
        Pre: poke_type entered must be either 'water','fire', or 'grass'
        Pre: level must be more than or equal to 1
        raises: value error when either condition is violated
        Complexity: O(1) for best and worst case
        """
        if level <= 0:
            raise ValueError("level must be more than or equal to 1")
        elif poke_type not in valid_types:
            raise ValueError("Pokemon must be of type 'fire','water' or 'grass'")
        else:
            effective_damage = self.cal_effective_damage(poke_type, level)
            if effective_damage > self.poke_defence*2:
                self.hp = self.hp - effective_damage
            else:
                self.hp = self.hp - effective_damage // 2
            return self.hp

    def get_defence(self):
        """ Gets the Defence of the pokemon 
            Complexity: O(1) for best and worst case
        """
        return self.poke_defence + self.level


class Charmander(PokemonBase):
    """Class Representing Charmander"""

    # initilaizing the class variables, these values are all the necessary pokemon stats (not including the operations that are required to be performed for some stats, this will be done in the methods)
    poke_name = "Charmander"
    poke_type = "fire"
    hp = 7
    poke_attack = 6
    poke_defence = 4
    poke_speed = 7

    def __init__(self) -> None:
        """Initialization"""
        PokemonBase.__init__(self, Charmander.hp, Charmander.poke_type)

    def pokemon_name(self) -> str:
        """Returns the name of the Pokemon
        Complexity: O(1) for best and worst"""
        return self.poke_name

    def get_speed(self) -> int:
        """Returns the speed of the pokemon
        Complexity: O(1) for best and worst"""
        return 7 + self.level

    def get_attack_damage(self) -> int:
        """Returns the attack damage
        Complexity: O(1) for best and worst"""
        return 6 + self.level

    def get_poke_type(self) -> str:
        """Returns the poke_type
        Complexity: O(1) for best and worst"""
        return self.poke_type

    def set_level(self, level) -> None:
        """Allows the user to set the level of Charmander
        Pre: Level must be more than or equal to 1
        raises: ValueError if level is not more than or equal to 1
        Complexity: O(1) for best and worst"""
        if level <= 0:
            raise ValueError("Level must be more than or equal to 1")
        else:
            self.level = level

    def get_level(self) -> int:
        """Returns the level
        Complexity: O(1) for best and worst"""
        return self.level


    def set_hp(self, hp) -> None:
        """Allows the user to set the HP
        Pre: HP must be more than 0
        raises: ValueError when HP<0
        Complexity: O(1) for best and worst"""
        if hp<0:
            raise ValueError("HP cannot be less than 0")
        else:
            self.hp = hp


    def get_hp(self) -> int:
        """Returns the HP
        Complexity: O(1) for best and worst"""
        return self.hp

    def cal_effective_damage(self, poke_type, level) -> float:
        """Calculates the effective damage charmander will take when battling a pokemon
        of a certain type. The user will enter the opponent's poke_type and level, and
        depending on these values the program will calculate the damage charmander will endure.
        Pre: poke_type entered must be either 'water','fire', or 'grass'
        Pre: level must be more than or equal to 1
        raises: value error when either condition is violated
        Complexity: O(1) for best and worst case"""
        if level <= 0:
            raise ValueError("level must be more than or equal to 1")
        elif poke_type not in valid_types:
            raise ValueError("Pokemon must be of type 'fire','water' or 'grass'")
        else:
            if poke_type == 'fire':
                return 1 * (6 + level)
            elif poke_type == 'water':
                return 2 * (4 +self.level //2)
            elif poke_type == 'grass':
                return 0.5 * (5)

    def get_damage_after_attack(self, poke_type, level) -> int:
        """Calculates the hp of Charmander when being attacked by a pokemon
        of a certain type and level. The user will enter the opponent's poke_type and level, and
        depending on these values the program will calculate the hp of the resultant attack.
        Pre: poke_type entered must be either 'water','fire', or 'grass'
        Pre: level must be more than or equal to 1
        raises: value error when either condition is violated
        Complexity: O(1) for best and worst case
        """
        if level <= 0:
            raise ValueError("level must be more than or equal to 1")
        elif poke_type not in valid_types:
            raise ValueError("Pokemon must be of type 'fire','water' or 'grass'")
        else:
            effective_damage = self.cal_effective_damage(poke_type, level)
            if effective_damage>self.poke_defence:
                self.hp=self.hp - effective_damage
            else:
                self.hp = self.hp - effective_damage //2
            return self.hp

    def get_defence(self):
        """ Gets the Defence of the pokemon 
            Complexity: O(1) for best and worst case
        """
        return self.poke_defence


class Bulbasaur(PokemonBase):
    """Class representing Bulbasaur"""

    # initilaizing the class variables, these values are all the necessary pokemon stats (not including the operations that are required to be performed for some stats, this will be done in the methods)
    poke_name = "Bulbasaur"
    poke_type = "grass"
    hp = 9
    poke_attack = 5
    poke_defence = 5
    poke_speed = 7

    def __init__(self) -> None:
        """Initialization"""
        PokemonBase.__init__(self, Bulbasaur.hp, Bulbasaur.poke_type)

    def pokemon_name(self) -> str:
        """Returns the name of the Pokemon
        Complexity: O(1) for best and worst"""
        return self.poke_name

    def get_speed(self) -> int:
        """Returns the speed of the pokemon
        Complexity: O(1) for best and worst"""
        return 7 + self.level // 2

    def get_attack_damage(self) -> int:
        """Returns the attack damage
        Complexity: O(1) for best and worst"""
        return self.poke_attack

    def get_poke_type(self) -> str:
        """Returns the poke_type
        Complexity: O(1) for best and worst"""
        return self.poke_type

    def set_level(self, level) -> None:
        """Allows the user to set the level of Charmander
        Pre: Level must be more than or equal to 1
        raises: ValueError if level is not more than or equal to 1
        Complexity: O(1) for best and worst"""
        if level <= 0:
            raise ValueError("Level must be more than or equal to 1")
        else:
            self.level = level

    def get_level(self) -> int:
        """Returns the level
        Complexity: O(1) for best and worst"""
        return self.level

    def set_hp(self, hp) -> None:
        """Allows the user to set the HP
        Pre: HP must be more than 0
        raises: ValueError when HP<0
        Complexity: O(1) for best and worst"""
        if hp<0:
            raise ValueError("HP cannot be less than 0")
        else:
            self.hp = hp

    def get_hp(self) -> int:
        """Returns the HP
        Complexity: O(1) for best and worst"""
        return self.hp

    def cal_effective_damage(self, poke_type, level) -> int:
        """Calculates the effective damage bulbasaur will take when battling a pokemon
        of a certain type. The user will enter the opponent's poke_type and level, and
        depending on these values the program will calculate the damage bulbasaur will endure.
        Pre: poke_type entered must be either 'water','fire', or 'grass'
        Pre: level must be more than or equal to 1
        raises: value error when either condition is violated
        Complexity: O(1) for best and worst case"""
        if level <= 0:
            raise ValueError("level must be more than or equal to 1")
        elif poke_type not in valid_types:
            raise ValueError("Pokemon must be of type 'fire','water' or 'grass'")
        else:
            if poke_type == 'fire':
                return 2 * (6 + level)
            elif poke_type == 'water':
                return 0.5 * (4 + level // 2)
            elif poke_type == 'grass':
                return 1 * (5)

    def get_damage_after_attack(self, poke_type, level)->int:
        """Calculates the hp of bulbasaur when being attacked by a pokemon
        of a certain type and level. The user will enter the opponent's poke_type and level, and
        depending on these values the program will calculate the hp of the resultant attack.
        Pre: poke_type entered must be either 'water','fire', or 'grass'
        Pre: level must be more than or equal to 1
        raises: value error when either condition is violated
        Complexity: O(1) for best and worst case
        """
        if level <= 0:
            raise ValueError("level must be more than or equal to 1")
        elif poke_type not in valid_types:
            raise ValueError("Pokemon must be of type 'fire','water' or 'grass'")
        else:
            effective_damage = self.cal_effective_damage(poke_type, level)
            if effective_damage>self.poke_defence+5:
                self.hp = self.hp - effective_damage
            else:
                self.hp = self.hp - effective_damage //2
            return self.hp

    def get_defence(self):
        """ Gets the Defence of the pokemon 
            Complexity: O(1) for best and worst case
        """
        return self.poke_defence


class GlitchMon(PokemonBase):
    """ Class representing the Glitch pokemon """
    # initilaizing the class variables, these values are all the necessary pokemon stats (not including the operations that are required to be performed for some stats, this will be done in the methods)
    poke_name = "Mysterious"
    poke_type = "None"
    hp = 8
    poke_attack = 5
    poke_defence = 5
    poke_speed = 7

    def __init__(self) -> None:
        """Initialization"""
        PokemonBase.__init__(self, GlitchMon.hp, GlitchMon.poke_type)

    def pokemon_name(self) -> str:
        """Returns the name of the Pokemon
        Complexity: O(1) for best and worst"""
        return self.poke_name


    def get_speed(self) -> int:
        """Returns the speed of the pokemon
        Complexity: O(1) for best and worst"""
        return self.poke_speed

    def get_attack_damage(self) -> int:
        """Returns the attack damage
        Complexity: O(1) for best and worst"""
        return self.poke_attack

    def get_poke_type(self) -> str:
        """Returns the poke_type
        Complexity: O(1) for best and worst"""
        return self.poke_type

    def set_level(self, level) -> None:
        """Allows the user to set the level of Pokemon
        Pre: Level must be more than or equal to 1
        raises: ValueError if level is not more than or equal to 1
        Complexity: O(1) for best and worst"""
        if level <= 0:
            raise ValueError("Level must be more than or equal to 1")
        else:
            self.level = level

    def get_level(self) -> int:
        """Returns the level
        Complexity: O(1) for best and worst"""
        return self.level

    def set_hp(self, hp) -> None:
        """Allows the user to set the HP
        Pre: HP must be more than 0
        raises: ValueError when HP<0
        Complexity: O(1) for best and worst"""
        if hp<0:
            raise ValueError("HP cannot be less than 0")
        else:
            self.hp = hp

    def get_hp(self) -> int:
        """Returns the HP
        Complexity: O(1) for best and worst"""
        return self.hp

    def cal_effective_damage(self, poke_type, level) -> float:
        """Calculates the effective damage Mysterious pokemon will take when battling a pokemon
        of a certain type. The user will enter the opponent's poke_type and level, and
        depending on these values the program will calculate the damage Mysterious pokemon will endure.
        Pre: level must be more than or equal to 1
        raises: value error when either condition is violated
        Complexity: O(1) for best and worst case"""
        if level <= 0:
            raise ValueError("level must be more than or equal to 1")
        else:
            if poke_type == 'fire':
                return 1 * (6 + level)
            elif poke_type == 'water':
                return 1 * (4 + level // 2)
            elif poke_type == 'grass':
                return 1 * (5)

    def get_damage_after_attack(self, poke_type, level) -> int:
        """Finds the after damage of Mysterious pokemon when being attacked by a pokemon
        of a certain type and level. The user will enter the opponent's poke_type and level, and
        depending on these values the program will calculate the hp of the resultant attack.
        Pre: level must be more than or equal to 1
        Raises: value error when the condition is violated
        Complexity: O(1) for best and worst case
        """
        if level <= 0:
            raise ValueError("level must be more than or equal to 1")
        else:
            random.choice((GlitchMon.after_damage1, GlitchMon.after_damage2, GlitchMon.after_damage3, GlitchMon.superpower))((self, poke_type, level)) #superpower 25% chance

    def after_damage1(self, poke_type, level):
        """
        squirtle
        """
        effective_damage = self.cal_effective_damage(poke_type, level)
        if effective_damage > self.poke_defence * 2:
            self.hp = self.hp - effective_damage
        else:
            self.hp = self.hp - effective_damage // 2
        return self.hp

    def after_damage2(self, poke_type, level):
        """
        bulbasaur
        """
        effective_damage = self.cal_effective_damage(poke_type, level)
        if effective_damage > self.poke_defence + 5:
            self.hp = self.hp - effective_damage
        else:
            self.hp = self.hp - effective_damage // 2
        return self.hp

    def after_damage3(self, poke_type, level):
        """
        Charmander
        """
        effective_damage = self.cal_effective_damage(poke_type, level)
        if effective_damage > self.poke_defence:
            self.hp = self.hp - effective_damage
        else:
            self.hp = self.hp - effective_damage // 2
        return self.hp

    def get_defence(self):
        """ Gets the Defence of the pokemon which increases on level up
        Complexity: O(1) for best and worst case
        """
        return self.poke_defence + self.level

    def increase_HP(self) -> int:
        """
        Increases the HP
        Pre: level must be more than or equal to 1
        Raises: value error when the condition is violated
        Complexity: O(1) for best and worst case
        """
        if self.level <= 0:
            raise ValueError("level must be more than or equal to 1")
        else:
            self.HP += 1
        return self.HP


    def superpower(self):
        """
        It has a random chance to chose any of the three effects when called:
        Gain 1 level
        - Gain 1 HP
        - Gain 1 HP and 1 level
        This method is called at a 25% chance every time the Pokemon has to defend from an attack.
        Pre: level must be more than or equal to 1
        Raises: value error when the condition is violated
        Complexity: O(1) for best and worst case
        """
        if self.level <= 0:
            raise ValueError("level must be more than or equal to 1")
        else:
            possible_effects = [1, 2, 3]
            effect = random.choice(possible_effects)
            if (effect == 1) :
                self.level += 1
            elif (effect == 2) :
                self.HP += 1
            elif (effect == 3) :
                self.HP += 1
                self.level += 1
        return self.HP and self.level



class MissingNo(GlitchMon):
    """ Class representing the Monster pokemon """
    # initilaizing the class variables, these values are all the necessary monster stats taken out manually by calculating the average (not including the operations that are required to be performed for some stats, this will be done in the methods)
    poke_name = "Monster"
    poke_type = "None"

    def stat_increase(self):
        """
        If level increases this function increases the stat
        Pre: level must be more than or equal to 1
        Raises: value error when the condition is violated
        Complexity: O(1) for best and worst case
        """
        if self.level <= 0:
            raise ValueError("level must be more than or equal to 1")
        else:
            self.hp += 1
            self.poke_attack += 1
            self.poke_defence += 1
            self.poke_speed += 1
        return self.hp, self.poke_attack, self.poke_defence, self.poke_speed

