"""
   Abstract Class: Pokemon Base Class
"""
__author__ = "Nabiha Siddiqui, Anushka Reddy"

from abc import ABC, abstractmethod


class PokemonBase(ABC):
    level= 1

    def __init__(self, hp, poke_type) -> None:
        """Initialization
        :pre: hp must be more than 0
        :pre: poke_type must be either 'grass', 'fire', or 'water'
        :raises: ValueError when user enters hp below 0, or when user enters type that is not fire, water or grass
        :complexity: O(1) for best and worst case
        """
        valid_types=["fire","water","grass"]
        if hp < 0:
            raise ValueError("HP must be more than 0")
        elif poke_type not in valid_types:
            raise ValueError("Pokemon must be of type 'fire','water' or 'grass'")
        else:
            self.hp = hp  # assigning the value of hp to the instance variable hp
            self.poke_type = poke_type  # assigning the poke_type string to the instance variable poke_type

    @abstractmethod
    def pokemon_name(self) -> str:
        """Returns name of pokemon"""
        pass

    @abstractmethod
    def get_speed(self) -> int:
        """Returns speed of pokemon"""
        pass

    @abstractmethod
    def get_attack_damage(self) -> int:
        """Returns attack damage of pokemon"""
        pass

    @abstractmethod
    def poke_type(self) -> str:
        """Returns poketype"""
        pass

    @abstractmethod
    def set_level(self, level) -> None:
        """Sets level of pokemon"""
        pass


    @abstractmethod
    def get_level(self) -> int:
        "Gets level of pokemon"
        pass

    @abstractmethod
    def set_hp(self, hp) -> None:
        """Sets hp of pokemon"""
        pass

    @abstractmethod
    def get_hp(self) -> int:
        """Gets hp of pokemon"""
        pass

    @abstractmethod
    def cal_effective_damage(self, poke_type, level) -> int:
        """Calculates the damage a pokemon takes when battling a pokemon of a certain type and level"""
        pass

    def __str__(self) -> str:
        """Prints the object of the class in a formatted string
        Complexity: O(1) for best and worst
        """
        return self.pokemon_name() + "'s HP = " + str(self.get_hp()) + " and " + "level = " + str(self.get_level())

