import unittest

from tester_base import TesterBase, captured_output
from battle import Battle
from pokemon import Charmander
from pokemon import Bulbasaur


class TestBattle(unittest.TestCase):

    def test_set_mode_battle(self):
        """Testing the output of set mode battle"""
        b = Battle("Ash", "Brock")
        # testing when two trainers have the same pokemon
        with captured_output("1 0 0\n1 0 0") as (inp, out, err):
            # Here, Ash gets a Charmander, and Brock gets a Charmander.
            #Since both trainers have the same pokemon and same amount of pokemon, the battle should result in a draw
            self.assertEqual(b.set_mode_battle(),"Draw")


    def test_rotating_mode_battle(self):
        """Testing the output of rotating mode battle"""
        b = Battle("Ash", "Brock")
        # testing when two trainers have the same pokemon
        with captured_output("2 1 0\n1 0 0") as (inp, out, err):
            # Here, Ash gets two Charmander and one Bulbasaur, and Brock gets a Charmander.
            self.assertEqual(b.rotating_mode_battle(), "Ash")

    def test_optimised_mode_battle(self):
        """Testing the output of optimised mode battle"""
        b = Battle("Ash", "Brock")
        # testing when two trainers have the same pokemon
        with captured_output("2 2 1\n0 2 1") as (inp, out, err):
            # Here, Ash gets two Charmander and two bulbasaur, and Brock gets two bulbasaur and one squirtle.
            self.assertEqual(b.optimised_mode_battle("hp", "lvl"), "Ash") #with the criterion of hp and level


if __name__ == '__main__':
    unittest.main()
