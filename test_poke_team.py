import unittest
from poke_team import PokeTeam
from tester_base import TesterBase, captured_output
from pokemon import Charmander


class TestPokeTeam(unittest.TestCase):
    """Class to test the methods of Poketeam"""

    def test_set_battle(self):
        """Testing if battle mode=4 will fail and will raise a ValueError"""
        team = PokeTeam("Ash")
        with self.assertRaises(ValueError):
            team.set_battle_mode(4)

    def test_assign_team(self):
        """Testing if the assign team method correctly assigns the team, where Charmander will be at the top of the stack"""
        team= PokeTeam("Ash")
        x=team.assign_team(1, 1, 0)
        poke=x.peek()
        self.assertEqual(poke.pokemon_name(), "Charmander")

    def test_choose_team(self):
        """Testing that when a correct team is input, then the team is allowed method should return True"""
        team= PokeTeam("Ash")
        with captured_output("2 2 1") as (inp, out, err):
            team.choose_team(0, None)
            self.assertTrue(team.team_is_allowed())
            
    def test_str(self):
        """Tests that the correct string is printed with a given input by the user"""
        team = PokeTeam("Ash")
        with captured_output("1 0 1") as (inp, out, err): #1 charmander and 1 squirtle
            team.choose_team(0, None)
            assert str(team) == "Charmander's HP = 7 and level = 1, Squirtle's HP = 8 and level = 1"

    def test_team(self):
        """Checks that the method team is allowed will return False when the team is of a size greater than 6"""
        team = PokeTeam("Ash")
        team.team=[7,0,0]
        self.assertFalse(team.team_is_allowed())


if __name__ == '__main__':
    unittest.main()
