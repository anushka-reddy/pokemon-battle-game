import unittest
from pokemon import Charmander, Bulbasaur, Squirtle
from pokemon_base import PokemonBase


class TestPokemon(unittest.TestCase):
    """Class to test the Pokemon file"""

    def test_get_hp(self):
        """Testing that the method get_hp will return the correct hp of each pokemon. O(1) for both cases."""
        c = Charmander()
        self.assertEqual(c.get_hp(), c.hp)
        b=Bulbasaur()
        self.assertEqual(b.get_hp(), b.hp)
        s=Squirtle()
        self.assertEqual(s.get_hp(),s.hp)

    def test_set_hp(self):
        """Testing that the method set_hp will correct raise a ValueError when a value<=0 is entered. This method also tests
        that when setting the hp, the value of the hp will be updated to the value that was setted. O(1) for both cases"""
        c = Charmander()
        with self.assertRaises(ValueError):#assert that there should be an exception raised when user inputs a value <=0
            c.set_hp(-5)
        c.set_hp(6)
        self.assertEqual(c.get_hp(), 6)
        b=Bulbasaur()
        b.set_hp(3)
        self.assertEqual(b.get_hp(), 3)
        s=Squirtle()
        s.set_hp(2)
        self.assertEqual(s.get_hp(), 2)

    def test_pokemon_name(self):
        """Testing that the correct names are returned for each pokemon. O(1) for both cases"""
        c = Charmander()
        self.assertEqual(c.pokemon_name(), 'Charmander')
        b=Bulbasaur()
        self.assertEqual(b.pokemon_name(), 'Bulbasaur')
        s=Squirtle()
        self.assertEqual(s.pokemon_name(), 'Squirtle')

    def test_get_speed(self):
        """Testing that the correct speeds are returned for each pokemon. O(1) for both cases"""
        c = Charmander()
        self.assertEqual(c.get_speed(), 7+c.level)
        b=Bulbasaur()
        self.assertEqual(b.get_speed(), 7+c.level//2)
        s=Squirtle()
        self.assertEqual(s.get_speed(), 7)

    def test_get_attack_damage(self):
        """Testing that the correct attack damages are returned for each pokemon. O(1) for both cases"""
        c = Charmander()
        self.assertEqual(c.get_attack_damage(), 6+c.level)
        b=Bulbasaur()
        self.assertEqual(b.get_attack_damage(), 5)
        s=Squirtle()
        self.assertEqual(s.get_attack_damage(), 4+s.level//2)

    def test_get_poke_type(self):
        """Testing that the correct poke_types are returned for each pokemon. O(1) for both cases"""
        c = Charmander()
        self.assertEqual(c.get_poke_type(), "fire")
        b=Bulbasaur()
        self.assertEqual(b.get_poke_type(), "grass")
        s=Squirtle()
        self.assertEqual(s.get_poke_type(), "water")

    def test_get_level(self):
        """Testing that the correct levels are returned for each pokemon. O(1) for both cases"""
        c=Charmander()
        self.assertEqual(c.get_level(), c.level)
        b=Charmander()
        self.assertEqual(b.get_level(), b.level)
        s=Charmander()
        self.assertEqual(s.get_level(), s.level)


    def test_set_level(self):
        """Testing that the correct levels will be returned, after setting them through the set_level method
        This method also tests that a value error will be raised if the user inputs a level <=0. O(1) for both cases"""
        c = Charmander()
        with self.assertRaises(ValueError): #assert that there should be an exception raised when user inputs a value <=0
            c.set_level(-1)
        c.set_level(5)
        self.assertEqual(c.get_level(), 5)
        b=Bulbasaur()
        b.set_level(3)
        self.assertEqual(b.get_level(), 3)
        s=Squirtle()
        s.set_level(6)
        self.assertEqual(s.get_level(), 6)

    def test_cal_effective_damage(self):
        """Tests the value returned by the calculate effective damage method is the correct value. The method cal_effective_damage should
         also raise a Value Error if the user inputs a level below or equal to 0, or if the user inputs a poke type that does not exist (in the pokemon class)
         O(1) for both cases"""
        c=Charmander()
        with self.assertRaises(ValueError):#assert that there should be an exception raised when user inputs a poke_type that does not exist in the pokemon class
            c.cal_effective_damage('water',0)
        self.assertEqual(c.cal_effective_damage('water',2), 8)
        b=Bulbasaur()
        self.assertEqual(b.cal_effective_damage('fire',2), 16)
        s=Squirtle()
        self.assertEqual(s.cal_effective_damage('grass',4), 10)

    def test_get_damage_after_attack(self):
        """Tests the damage calculated that a pokemon recieves when being attacked by a pokemon of a specific type of level. The method tests
        that a value error is ouput when the user inputs a poke type or level that does not exist. O(1) for both cases."""
        c=Charmander()
        with self.assertRaises(ValueError):
            c.get_damage_after_attack('ice',3)
        self.assertEqual(c.get_damage_after_attack('grass',1),6.0)
        b=Bulbasaur()
        self.assertEqual(b.get_damage_after_attack('water',1),8.0)
        s=Squirtle()
        self.assertEqual(s.get_damage_after_attack('fire',1),7.0)

    def test_get_defence(self):
        """Tests that the defence method will return the correct defense value for each pokemon. O(1) for both."""
        c=Charmander()
        self.assertEqual(c.get_defence(), c.poke_defence)
        b=Bulbasaur()
        self.assertEqual(b.get_defence(), b.poke_defence)
        s=Squirtle()
        self.assertEqual(s.get_defence(), s.poke_defence+s.level)


if __name__ == '__main__':
    unittest.main()
