from pokemon import Charmander
import unittest
from pokemon_base import PokemonBase

class TestPokeBase(unittest.TestCase):

    def test_str(self):
        """Tests that the correct string is printed with a given input by the user"""
        c=Charmander()
        assert str(c) == "Charmander's HP = 7 and level = 1"
            
if __name__ == '__main__':
    unittest.main()
